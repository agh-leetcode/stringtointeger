import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CheckedInputStream;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        String a = "      -11919730356x";
        String b = "    -42";
        String c = "4193 with words";
        String d = "words and 987";
        String e = "91283472332";
        String f = "-+1";
        String ff= "3.14435";
        String gg= "  -0012a42";

        System.out.println(myAtoi(a));
    }

    private int myAtoi(String str){
        int head = 0;
        boolean positive = true;
        int currentNum = 0;
        char num;
        while (head<str.length() && str.charAt(head)== ' ')
            head++;

        if (head == str.length())
            return 0;

        if (str.charAt(head)== '-' || str.charAt(head)== '+'){
            if (str.charAt(head) == '-')
                positive = false;
            head++;
        }

        while (head<str.length() && (str.charAt(head)>= '0' && str.charAt(head)<='9')){
            if (currentNum > Integer.MAX_VALUE/10 || (currentNum == Integer.MAX_VALUE/10 && str.charAt(head) - '0' > 7)){
                if (positive) return Integer.MAX_VALUE;
                return Integer.MIN_VALUE;
            }
            currentNum = currentNum*10 + str.charAt(head) - '0';
            head++;
        }

        currentNum = (positive)? currentNum: currentNum*-1;
        return currentNum;
    }

  /*  private int myAtoi(String str) {
        if (str.isEmpty() || str.matches("\\s*"))
            return 0;
        String s = str.trim();
        if (s.matches("^([\\+\\-\\d])(.+)?")) {
            StringBuilder stringBuilder = new StringBuilder();
            char[] chars = s.toCharArray();
            if (s.matches("^\\d+(.+)?")){
                for (int i = 0; i < chars.length; i++) {
                    if (Character.isDigit(chars[i])) {
                        stringBuilder.append(chars[i]);
                    }else {
                        break;
                    }
                }
            } else if (s.matches("^[\\-\\+]\\d+(.+)?") && s.length()>1){
                stringBuilder.append(chars[0]);
                for (int i = 1; i < chars.length; i++) {
                    if (Character.isDigit(chars[i])) {
                        stringBuilder.append(chars[i]);
                    }else {
                        break;
                    }
                }
            } else {
                return 0;
            }
            try {
                return Integer.valueOf(stringBuilder.toString());
            } catch (NumberFormatException ex) {
                if (stringBuilder.toString().startsWith("-")) {
                    return Integer.MIN_VALUE;
                } else {
                    return Integer.MAX_VALUE;
                }
            }
        }
        return 0;
    }*/
}

